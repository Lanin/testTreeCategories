<?php

session_start();
define('ROOT_DIR', __DIR__ .'/');

include_once ROOT_DIR . 'core/autoload.php';


$router  = new Core\Routers\Router(ROOT_DIR.'config.php');
$router->run();
